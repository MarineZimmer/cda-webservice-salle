package fr.afpa.salleafpa.controlleurs;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.function.EntityResponse;

import fr.afpa.salleafpa.jwt.JwtUtils;
import fr.afpa.salleafpa.metier.entities.Personne;
import fr.afpa.salleafpa.metier.entities.Reservation;
import fr.afpa.salleafpa.metier.iservices.IServicePersonne;
import fr.afpa.salleafpa.outils.ControleSaisie;
import fr.afpa.salleafpa.outils.Parametrage;

@RestController
//@CrossOrigin(origins = "http://127.0.0.1:5500")
@CrossOrigin(origins = "*")
public class RestControllerPersonne {

	private static final Logger logger = LoggerFactory.getLogger(RestControllerPersonne.class);

	@Autowired
	private IServicePersonne servPers;

	@PostMapping(value = "/personne")
	public ResponseEntity<?> creationPersooneWS(@RequestBody Personne personne, @RequestHeader HttpHeaders header) {

		boolean ok = true;
		Object reponse;
		HttpStatus status;
		Map<String, String> map = new HashMap<String, String>();

		final Optional<List<String>> headerAutorisation = Optional.ofNullable(header.get(HttpHeaders.AUTHORIZATION));

		if (headerAutorisation.isPresent()) {
			final Optional<String> token = Optional.ofNullable(headerAutorisation.get().get(0));

			if (token.isPresent() && JwtUtils.isAuthentificate(token.get(), true)) {

				if (!ControleSaisie.isMail(personne.getMail())) {
					ok = false;
					map.put("mailKO", "*mail invalide");
				}
				if (!ControleSaisie.isNumTel(personne.getTel())) {
					ok = false;
					map.put("telKO", "*10 chiffres sans espace");
				}
				if (!ControleSaisie.isNonVide(personne.getAuthentification().getLogin())
						|| servPers.getPersonneByLogin(personne.getAuthentification().getLogin()) != null) {
					ok = false;
					map.put("loginKO", "*login déjà existant ou non valide");
				}
				if (!ControleSaisie.isNonVide(personne.getAuthentification().getMdp())) {
					ok = false;
					map.put("mdpKO", "*mot de passe non vide");
				}
				if (!ControleSaisie.isNonVide(personne.getAdresse())) {
					ok = false;
					map.put("adresseKO", "*adresse non vide");
				}
				if (!ControleSaisie.isDateValideWS(personne.getDateDeNaissance())) {
					ok = false;
					map.put("dateKO", "*age minimum est 16 ans, age maximum 80 ans");
				}
				if (!ControleSaisie.isNom(personne.getPrenom())) {
					ok = false;
					map.put("prenomKO", "*uniquement des lettres sans accent");
				}
				if (!ControleSaisie.isNom(personne.getNom())) {
					ok = false;
					map.put("nomKO", "*uniquement des lettres sans accent");
				}
				if (personne.getRole().getId() == 0) {
					ok = false;
					map.put("roleKO", "*définir un role");
				}
				if (personne.getFonction().getId() == 0) {
					ok = false;
					map.put("fonctionKO", "*définir une fonction");
				}

				if (!ok) {
					status = HttpStatus.BAD_REQUEST;
					reponse = map;
				} else {
					status = HttpStatus.OK;
					reponse = servPers.creationPersonneWS(personne);

				}
				return (ResponseEntity<?>) ResponseEntity.status(status).body(reponse);

			}
		}
		status = HttpStatus.UNAUTHORIZED;
		return ResponseEntity.status(status).body("unauthorized");

	}

	@PutMapping(value = "/personne")
	public ResponseEntity<?> updatePersooneWS(@RequestBody Personne personne, @RequestHeader HttpHeaders header) {

		HttpStatus status;
		boolean ok = true;
		Object reponse;
		Map<String, String> map = new HashMap<String, String>();

		final Optional<List<String>> headerAutorisation = Optional.ofNullable(header.get(HttpHeaders.AUTHORIZATION));

		if (headerAutorisation.isPresent()) {
			final Optional<String> token = Optional.ofNullable(headerAutorisation.get().get(0));

			if (token.isPresent() && JwtUtils.isAuthentificate(token.get(), true)) {
				if (!ControleSaisie.isMail(personne.getMail())) {
					ok = false;
					map.put("mailKO", "*mail invalide");
				}
				if (!ControleSaisie.isNumTel(personne.getTel())) {
					ok = false;
					map.put("telKO", "*10 chiffres sans espace");
				}

				if (!ControleSaisie.isNonVide(personne.getAuthentification().getMdp())) {
					ok = false;
					map.put("mdpKO", "*mot de passe non vide");
				}
				if (!ControleSaisie.isNonVide(personne.getAdresse())) {
					ok = false;
					map.put("adresseKO", "*adresse non vide");
				}
				if (!ControleSaisie.isDateValideWS(personne.getDateDeNaissance())) {
					ok = false;
					map.put("dateKO", "*age minimum est 16 ans, age maximum 80 ans");
				}
				if (!ControleSaisie.isNom(personne.getPrenom())) {
					ok = false;
					map.put("prenomKO", "*uniquement des lettres sans accent");
				}
				if (!ControleSaisie.isNom(personne.getNom())) {
					ok = false;
					map.put("nomKO", "*uniquement des lettres sans accent");
				}
				if (servPers.getPersonneByLogin(personne.getAuthentification().getLogin()) == null) {
					ok = false;
					map.put("nomKO", "*uniquement des lettres sans accent");
				}

				if (!ok) {
					status = HttpStatus.BAD_REQUEST;
					reponse = map;
				} else {
					status = HttpStatus.OK;
					reponse = servPers.updatePersonneWS(personne);
				}
				return (ResponseEntity<?>) ResponseEntity.status(status).body(reponse);
			}
		}
		status = HttpStatus.UNAUTHORIZED;
		return ResponseEntity.status(status).body("unauthorized");

	}

	@GetMapping(value = "/personne/{login}")
	public ResponseEntity<?> getPersonneWS(@PathVariable("login") String login,@RequestHeader HttpHeaders header)
	{

		HttpStatus status;
		final Optional<List<String>> headerAutorisation = Optional.ofNullable(header.get(HttpHeaders.AUTHORIZATION));

		if (headerAutorisation.isPresent()) {
			final Optional<String> token = Optional.ofNullable(headerAutorisation.get().get(0));

			if (token.isPresent() && JwtUtils.isAuthentificate(token.get(), true)) {
				Personne personne = servPers.getPersonneByLogin(login);
				if (personne != null) {
					status = HttpStatus.OK;
				} else {
					status = HttpStatus.BAD_REQUEST;
				}
				return ResponseEntity.status(status).body(personne);
			}
		}
		status = HttpStatus.UNAUTHORIZED;
		return ResponseEntity.status(status).body("unauthorized");

	}

	@GetMapping(value = "/personne")
	public ResponseEntity<?> getListePersonnesWS(@RequestHeader HttpHeaders header) {
		HttpStatus status;
		final Optional<List<String>> headerAutorisation = Optional.ofNullable(header.get(HttpHeaders.AUTHORIZATION));

		if (headerAutorisation.isPresent()) {
			final Optional<String> token = Optional.ofNullable(headerAutorisation.get().get(0));

			if (token.isPresent() && JwtUtils.isAuthentificate(token.get(), true)) {
		List<Personne> liste =  servPers.getAllPersonnes();
		status = HttpStatus.OK;
		return ResponseEntity.status(status).body(liste);
			}
		}
		status = HttpStatus.UNAUTHORIZED;
		return ResponseEntity.status(status).body("unauthorized");

	}
	@GetMapping(value = "/personnepage/{page}")
	public ResponseEntity<?> presonnesGet(@PathVariable("page") String page, @RequestHeader HttpHeaders header) {

		Map<String, Object> map = new HashMap<String, Object>();
		HttpStatus status;
		final Optional<List<String>> headerAutorisation = Optional.ofNullable(header.get(HttpHeaders.AUTHORIZATION));

		if (headerAutorisation.isPresent()) {
			final Optional<String> token = Optional.ofNullable(headerAutorisation.get().get(0));

			if (token.isPresent() && JwtUtils.isAuthentificate(token.get(), true)) {

				int pageEnCours = 0;
				int nbPage = servPers.nbPage(Parametrage.NB_PERSONNE_PAGE);
				if (ControleSaisie.isIntegerPositif(page)) {
					pageEnCours = Integer.parseInt(page) - 1;
				}
				if (pageEnCours < 0) {
					pageEnCours = 0;
				} else if (pageEnCours > nbPage - 1) {
					pageEnCours = nbPage - 1;
				}

				List<Personne> listePersonne = servPers.getAll(pageEnCours, Parametrage.NB_PERSONNE_PAGE);
				map.put("listeUsers", listePersonne);
				map.put("nbPage", nbPage);

				if (listePersonne != null) {
					status = HttpStatus.OK;
					return ResponseEntity.status(status).body(map);
				} else {
					status = HttpStatus.INTERNAL_SERVER_ERROR;
					return ResponseEntity.status(status).body("erreur");
				}
			}
		}
		status = HttpStatus.UNAUTHORIZED;
		return ResponseEntity.status(status).body("unauthorized");
	}

	@DeleteMapping(value = "/personne/{login}")
	public ResponseEntity<?> deletePersonneWS(@PathVariable("login") String login, @RequestHeader HttpHeaders header) {
		HttpStatus status;
		final Optional<List<String>> headerAutorisation = Optional.ofNullable(header.get(HttpHeaders.AUTHORIZATION));

		if (headerAutorisation.isPresent()) {
			final Optional<String> token = Optional.ofNullable(headerAutorisation.get().get(0));

			if (token.isPresent() && JwtUtils.isAuthentificate(token.get(), true)) {

				Object personne = servPers.deletePersonneWS(login);

				if (personne != null) {
					status = HttpStatus.OK;
				} else {
					status = HttpStatus.BAD_REQUEST;
					personne = "un administrateur ne peut pas être supprimé";
				}
				return (ResponseEntity<?>) ResponseEntity.status(status).body(personne);

			}
		}
		status = HttpStatus.UNAUTHORIZED;
		return ResponseEntity.status(status).body("unauthorized");

	}

}
