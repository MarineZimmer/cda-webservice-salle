package fr.afpa.salleafpa.controlleurs;

import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RestController;

import fr.afpa.salleafpa.jwt.JwtUtils;
import fr.afpa.salleafpa.metier.entities.Fonction;
import fr.afpa.salleafpa.metier.entities.Role;
import fr.afpa.salleafpa.metier.iservices.IServiceRole;

@RestController

@CrossOrigin(origins = "*")
public class RestControllerRole {

	private static final Logger logger = LoggerFactory.getLogger(RestControllerRole.class);

	@Autowired
	private IServiceRole servRole;

	@PostMapping(value = "/role")
	public Role creationRoleWS(@RequestBody Role role) {
		return servRole.creationRole(role);

	}

	@PutMapping(value = "/role")
	
	public Role updateRoleWS(@RequestBody Role role) {

		return servRole.updateRole(role);

	}

	@GetMapping(value = "/role/{id}")
	public Role getRoleWS(@PathVariable("id") Integer id) {
		return servRole.getRole(id);
	
	}

	@GetMapping(value = "/role")
	public ResponseEntity<?> getListeRolesWS(@RequestHeader HttpHeaders header) {
		HttpStatus status;
		final Optional<List<String>> headerAutorisation = Optional.ofNullable(header.get(HttpHeaders.AUTHORIZATION));
		if (headerAutorisation.isPresent()) {
			
			final Optional<String> token = Optional.ofNullable(headerAutorisation.get().get(0));
			
			if (token.isPresent() && JwtUtils.isAuthentificate(token.get(), false)) {
				
				List<Role> listeRole = servRole.getAllRole();
				
				if(listeRole!=null) {
					
				status = HttpStatus.OK;
				return ResponseEntity.status(status).body(listeRole);
				}else {
					status = HttpStatus.INTERNAL_SERVER_ERROR;
					return ResponseEntity.status(status).body("erreur");
				}
			}
		}
		status = HttpStatus.UNAUTHORIZED;
		return ResponseEntity.status(status).body("unauthorized"); 
		

	}

	@DeleteMapping(value = "/role/{id}")
	public Role deleteRoleWS( @PathVariable("id") Integer id) {
		return servRole.deleteRole(id);


	}

}
