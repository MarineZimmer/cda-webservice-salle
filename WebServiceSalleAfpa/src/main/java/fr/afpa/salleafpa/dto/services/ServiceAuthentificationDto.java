package fr.afpa.salleafpa.dto.services;

import java.util.Optional;

import org.mindrot.jbcrypt.BCrypt;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.afpa.salleafpa.dao.entities.AuthentificationDao;
import fr.afpa.salleafpa.dao.repositories.AuthentificationRepository;
import fr.afpa.salleafpa.dto.iservices.IServiceAuthentificationDto;
import fr.afpa.salleafpa.metier.entities.Authentification;
import fr.afpa.salleafpa.metier.entities.Personne;

@Service
public class ServiceAuthentificationDto implements IServiceAuthentificationDto {
	
	@Autowired
	private AuthentificationRepository authDao;
	
	@Override
	public Personne authentification(String login, String mdp) {
		Personne personneAuth = null;
		
		Optional<AuthentificationDao> authDaoRecup = authDao.findByLogin(login);
		if (authDaoRecup.isPresent() && BCrypt.checkpw(mdp, authDaoRecup.get().getMdp())) {
			personneAuth = ServicePersonneDto.personneDaoToPersonneMetier(authDaoRecup.get().getPersonne());
		}
		return personneAuth;
	}

	/**
	 * methode qui permet de transformer une entité authentification métier en
	 * entité authentificationDao
	 * 
	 * @param auth : entité authentification métier à transformer
	 * @return : une entité authentificationDao correspondante à l'entité
	 *         authentification métier
	 */
	public static AuthentificationDao authMetierToAuthDao(Authentification auth) {
		AuthentificationDao authDao = new AuthentificationDao();
		authDao.setLogin(auth.getLogin());
		String mdp =auth.getMdp();
		authDao.setMdp(BCrypt.hashpw(mdp, BCrypt.gensalt(12)));
		return authDao;
	}

	/**
	 * méthode qui permet de transformer une entité authentificationDao en entité
	 * authentification métier
	 * 
	 * @param auth : entité authentificationDao à transformer
	 * @return : une entité authentification métier correspondante à l'entité
	 *         authentificationDao
	 */
	public static Authentification authDaoToAuthMetier(AuthentificationDao authDao) {
		Authentification auth = new Authentification(authDao.getLogin(), authDao.getMdp());
		return auth;
	}

	

}
