package fr.afpa.salleafpa.dto.iservices;

import java.util.List;

import fr.afpa.salleafpa.metier.entities.TypeMateriel;

public interface IServiceTypeMaterielDto {
	
	TypeMateriel creationTypeMateriel(TypeMateriel typeMateriel);

	TypeMateriel updateTypeMateriel(TypeMateriel typeMateriel);

	TypeMateriel getTypeMateriel(Integer id);

	List<TypeMateriel> getAllTypeMateriel();

	TypeMateriel deleteTypeMateriel(Integer id);
	
	/**
	 * dto qui retourne le nombre de page necessaire pour afficher les types materiel 
	 * @param nbTypeMaterielPage : le nombre de types materiel par page
	 * @return le nombre de page pour afficher les types de materiel
	 */
	int nbPageListeMateriel(int nbTypeMaterielPage);

	List<TypeMateriel> getAllTypeMateriel(int page, int nbTypeMaterielPage);
}
