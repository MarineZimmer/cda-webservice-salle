package fr.afpa.salleafpa.metier.entities;

import java.util.List;

import org.springframework.web.multipart.MultipartFile;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.FieldDefaults;


@FieldDefaults(level = AccessLevel.PRIVATE)
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class Salle {

	Integer idSalle;
	Integer numero;
	String nom;
	Float surface;
	Integer capacite;
	Integer etage;
	boolean actif;
	TypeSalle typeSalle;
	Batiment batiment;
	List<Reservation> listeReservation;
	List<Materiel> listeMateriel;
	String cheminImage;
	
	
	public Salle(int idSalle, int numero, String nom, float surface, int capacite, int etage, boolean actif) {
		super();
		this.idSalle = idSalle;
		this.numero = numero;
		this.nom = nom;
		this.surface = surface;
		this.capacite = capacite;
		this.etage = etage;
		this.actif = actif;
	}


	@Override
	public String toString() {
		return "Salle [idSalle=" + idSalle + ", numero=" + numero + ", nom=" + nom + ", surface=" + surface
				+ ", capacite=" + capacite + ", etage=" + etage + ", actif=" + actif + ", typeSalle=" + typeSalle
				+ ", batiment=" + batiment + ", listeReservation=" + listeReservation + ", listeMateriel="
				+ listeMateriel + "]";
	}
	
	
	

}
