package fr.afpa.salleafpa.dao.repositories;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import fr.afpa.salleafpa.dao.entities.AuthentificationDao;
import fr.afpa.salleafpa.dao.entities.ReservationDao;

public interface AuthentificationRepository extends JpaRepository <AuthentificationDao, String>{

	Optional<AuthentificationDao> findByLoginAndMdp(String login, String mdp);

	Optional<AuthentificationDao> findByLogin(String login);

}
