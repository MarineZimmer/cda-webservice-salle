package fr.afpa.salleafpa.dao.repositories;



import org.springframework.data.jpa.repository.JpaRepository;

import fr.afpa.salleafpa.dao.entities.FonctionDao;

public interface FonctionRepository extends JpaRepository <FonctionDao, Integer> {
	
	

}