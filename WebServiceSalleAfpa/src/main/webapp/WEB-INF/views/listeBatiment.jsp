<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@page import="fr.afpa.salleafpa.outils.Parametrage"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Liste de bâtiments</title>
<link href="${pageContext.request.contextPath}/resources/css/menu.css" rel="stylesheet">
<link href="${pageContext.request.contextPath}/resources/css/cssListeBatiment.css" rel="stylesheet">
</head>
<body>
<jsp:include page="menu.jsp" />

 <div class="container">
 <div>
	<h1>Liste de Bâtiments</h1>
</div>
	<div class="container-fluid col-sm-12">
			<table class="table table-striped">
		<tr>
			<th>Identifiant</th>
			<th>Nom</th>
		</tr>
		<c:forEach items="${ listeDeBatiment }" var="liste">

			<tr>
				<th><c:out value=" ${ liste.id }" /></th>
				<th><c:out value=" ${ liste.nom }" /></th>
				<c:url value="/modifier" var="modif">
					<c:param name="id" value="${ liste['id'] }" />
					<c:param name="nom" value="${ liste['nom'] }" />
					</c:url>
					<td><a href="${ modifier }"><button class="btntable">Modifier</button></a></td>
			</tr>


		</c:forEach>
		</table>
<!--  	<a href="ajouterBatiment" >Ajouter un batiment</a> -->
		<%--<a href='<c:url value="${ Parametrage.URI_AJOUTER_BATIMENT}" />'> Ajouter un bâtiment </a>--%>
		
		<h2>Création d'un nouveau bâtiment</h2>   
		<form action = "${Parametrage.URI_AJOUTER_BATIMENT}"method="post">
		  <!--COLONNE GAUCHE-->
                    <div class="colonne">
                    
                 
                    
                        <!--partie nom-->
                        <div class="colonneGauche"> 
                            <label>Nom :</label>
                        </div>
                        <div class="colonneDroite"> 
                            <input name="nom" value="${nom}" type="text" required="">
                             <br>
                            <em>${nomKO }</em>
                        </div>
                       </div> 
                         <!--partie BOUTTON VALIDER-->
                        <div class="colonneGauche"> 
                            <label></label>
                        </div>
                        <div class="colonneDroite"> 
                            <button type="submit" value="Valider">Valider</button>
                        </div>
                        
                        
                        <!--partie BOUTTON ANNULER retour vers l'accueil à integrer-->
                        <div class="colonneGauche"> 
                            <label></label>
                        </div>
                        <div class="colonneDroite"> 
                            <button type="reset" value="Annuler">Annuler</button>
                        </div>
                        </form>
                        
                       </div>
                       </div>
                       <script
		src="${pageContext.request.contextPath}/resources/js/jquery-3.4.1.slim.js"></script>
	<script
		src="${pageContext.request.contextPath}/resources/js/bootstrap.bundle.min.js"></script>
				
</body>
</html>