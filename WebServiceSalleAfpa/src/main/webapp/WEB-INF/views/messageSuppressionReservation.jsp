<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
  <%@page import="fr.afpa.salleafpa.outils.Parametrage"%>
  <%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>

<meta charset="ISO-8859-1">
<title>admin</title>

<link href ="${pageContext.request.contextPath}/resources/css/cssMessageSupReservation.css" rel="stylesheet">
<link href="${pageContext.request.contextPath}/resources/css/menu.css"
	rel="stylesheet">
</head>
<body>
	<c:choose>
		<c:when
			test="${sessionScope.persAuthSalle.role.id eq Parametrage.ADMIN_ID }">
		<jsp:include page="menu.jsp" />		
<flex-container>

          
                <label for="message">${message }</label>
                <br> 
                
             
              
                <br> 
                <br> 
                <br> 
            </form>
 </flex-container>
		</c:when>
		<c:otherwise>
			<c:redirect url=""></c:redirect>
		</c:otherwise>
	</c:choose>
</body>
</html>